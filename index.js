"use strict"
const csv = require('csv-parser')
const path = require('path')
const fs = require('fs')
const escape = require('sql-escape');
const argv = require('yargs').argv

const inputDir = argv.input||'input'
const outputDir = argv.output||'output'

if (!fs.existsSync(inputDir)){
  fs.mkdirSync(inputDir, {recursive: true})
}

if (!fs.existsSync(outputDir)){
  fs.mkdirSync(outputDir, {recursive: true})
}

fs.readdir(inputDir, {withFileTypes: true}, (err, files) => {
  files.forEach(file => {
    convertFile(path.join('input', file.name))
  })
})

function convertFile (inputFile) {
  const maxLines = argv.maxLines||1000000000
  const tableName = path.basename(inputFile.split('.').slice(0, -1).join('.'));
  let outputFile = path.join('output',  path.basename(inputFile) + '.sql')

  let isFirstLine = true
  let counter = 0
  let currentLine = 0
  let isNewFile = true

  
  
  fs.createReadStream(inputFile)
    .pipe(csv())
    .on('data', (row) => {

      if (isNewFile) {
        if (fs.existsSync(outputFile)) {
          fs.unlinkSync(outputFile)
        }

        if(argv.truncate && counter == 0) {
          fs.appendFileSync(outputFile, `TRUNCATE TABLE \`${tableName}\`;\n`)
        }

        fs.appendFileSync(outputFile, `INSERT INTO \`${tableName}\` VALUES \n`)
        isNewFile = false
      }

      const cols = []
      for (const key in row) {
        cols.push(`"${escape(row[key])}"`)
      }
      fs.appendFileSync(outputFile, `${isFirstLine ? '': '\n,'}(${cols.join(',')})`)
      isFirstLine = false
      currentLine++
      
      if (currentLine > maxLines - 1) {
        fs.appendFileSync(outputFile, ';')
        outputFile = path.join('output',  `${path.basename(inputFile)}_${++counter}.sql`)
        isNewFile = true
        isFirstLine = true
        currentLine = 0
        
      }
    })
    .on('end', () => {
      fs.appendFileSync(outputFile, ';')
      console.log(inputFile + '  successfully processed')
    })
}